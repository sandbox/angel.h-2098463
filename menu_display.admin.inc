<?php

/**
 * @file
 * Administrative page callbacks for Menu Display module.
 */

/**
 * Page callback for the overview of the menu display interface.
 */
function menu_display_main_interface() {
  // Load all existing Menu display entities.
  $menu_displays = entity_load('menu_display');
  dpm($menu_displays);

  $header = array(t('Title'), array(
    'data' => t('Operations'),
    'colspan' => '2',
    ));
  $rows = array();
  foreach ($menu_displays as $menu_display) {
    $row = array($menu_display->title . '<div class="description">' . check_plain($menu_display->description) . '</div>');
    $row[] = array('data' => l(t('edit menu display'), 'admin/structure/menu_displays/' . $menu_display->mdid . '/edit'));
    $row[] = array('data' => l(t('delete menu display'), 'admin/structure/menu_displays/' . $menu_display->mdid . '/delete'));
    $rows[] = $row;
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Page callback for the add/edit of a Reveal.js menu.
 */
function menu_display_edit_page($form, &$form_state, $menu_display = NULL) {
  // Prepare some variables for the default values.
  $mdid = !empty($menu_display) ? $menu_display->mdid : 0;

  if (empty($mdid)) {
    $menu_display = menu_display_empty_menu_display();
  }
  dpm($menu_display);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu display title'),
    '#default_value' => $menu_display->title,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $menu_display->description,
    '#description' => t('Short description which will appear in the list of the menu displays.'),
  );

  $form['menu_name'] = array(
    '#type' => 'select',
    '#title' => t('Menus'),
    '#options' => menu_get_menus(),
    '#default_value' => !empty($menu_display->menu_name) ? $menu_display->menu_name : 'main-menu',
    '#description' => t('Select the menu of which you want the menu display.'),
    '#required' => TRUE,
  );

  $levels = drupal_map_assoc(range(1, 9));
  $levels['curr_lvl'] = t('Current level');
  $levels['curr_lvl_plus_1'] = t('Current level + 1');

  $form['from_depth'] = array(
    '#type' => 'select',
    '#title' => t('From level'),
    '#default_value' => $menu_display->from_depth,
    '#options' => $levels,
    '#description' => t('From which level of the menu the rendering should start.'),
  );

  $form['to_depth'] = array(
    '#type' => 'select',
    '#title' => t('To level'),
    '#default_value' => $menu_display->to_depth,
    '#options' => $levels,
    '#description' => t('On which level of the menu the rendering should end.'),
  );

  $form['styling'] = array(
    '#type' => 'fieldset',
    '#title' => t('Styling'),
    '#collapsable' => TRUE,
  );

  $form['styling']['add_active_class'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add active class'),
    '#default_value' => $menu_display->styling_options['add_active_class'],
    '#description' => t('Check to have class "active" on the <em>li</em> of the active menu item.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('menu_display_edit_submit'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('menu_display_edit_cancel'),
  );

  $form['mdid'] = array(
    '#type' => 'hidden',
    '#value' => $mdid,
  );

  return $form;
}

/**
 * Submit callback for the cancel button of the edit Menu display form.
 */
function menu_display_edit_cancel($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/menu_displays';
}

/**
 * Submit callback for the edit operation of the Menu display form.
 */
function menu_display_edit_submit($form, &$form_state) {
  // Save the Menu display entity.
  $mdid = isset($form_state['values']['mdid']) ? $form_state['values']['mdid'] : 0;

  $fields = array('title', 'name', 'description', 'menu_name', 'ignore_active_trail', 'active_items', 'from_depth', 'to_depth', 'no_links');
  $styling_options = array('add_active_class');

  $entity = array();
  foreach ($fields as $field) {
    if (!empty($form_state['values'][$field])) {
      $entity[$field] = $form_state['values'][$field];
    }
  }
  foreach ($styling_options as $option) {
    $entity['styling_options'][$option] = $form_state['values'][$option];
  }
  $entity['styling_options'] = serialize($entity['styling_options']);

  if (!empty($mdid)) {
    $entity['mdid'] = $mdid;
  }
  else {
    $entity = entity_create('menu_display', $entity);
  }

  entity_save('menu_display',(object) $entity);

  drupal_set_message(t('Menu display saved.'));
  $form_state['redirect'] = 'admin/structure/menu_displays';
}

/**
 * Page callback for the delete operation of menu display.
 */
function menu_display_delete_form($form, &$form_state, $menu_display) {
  $form_state['menu_display'] = $menu_display;
  return confirm_form($form,
    t('Are you sure you want to delete the Reveal JS menu %name?', array('%name' => $menu_display->title)),
    'admin/structure/menu_displays',
    t('This action cannot be undone.'),
    t('Delete'));
}

/**
 * Delete callback.
 */
function menu_display_delete_form_submit($form, &$form_state) {
  entity_delete('menu_display', $form_state['menu_display']->mdid);
  drupal_set_message(t('Reveal JS menu deleted.'));
  $form_state['redirect'] = 'admin/structure/menu_displays';
}
